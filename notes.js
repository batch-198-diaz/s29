db.products.insertMany(
	[
		{
			"name": "Iphone X",
			"price": 30000,
			"isActive": true
		},
		{
			"name": "Samsung Galaxy S21",
			"price": 51000,
			"isActive": true
		},
		{
			"name": "Razer Blackshark V2X",
			"price": 2800,
			"isActive": false
		},
		{
			"name": "RAKK Gaming Mouse",
			"price": 1800,
			"isActive": true
		},
		{
			"name": "Razer Mechanical Keyboard",
			"price": 4000,
			"isActive": true
		}
	]
)



/* Query Operators

	- Allows us to expand our queries and define conditions instead of just looking specific values
	- $gt, $lt, $gte, $lte
*/

db.products.find({price:{$gt: 3000}})
db.products.find({price:{$lt: 3000}})

db.users.insertMany(
	[
		{
			"firstname": "Mary Jane",
			"lastname": "Watson",
			"email": "mjtiger@gmail.com",
			"password": "tigerjackpot15",
			"isAdmin": false
		},
		{
			"firstname": "Gwen",
			"lastname": "Stacy",
			"email": "stacyTech@gmail.com",
			"password": "stacyTech1991",
			"isAdmin": true
		},
		{
			"firstname": "Peter",
			"lastname": "Parker",
			"email": "peterWebDev@gmail.com",
			"password": "webdeveloperPeter",
			"isAdmin": true
		},
		{
			"firstname": "Jonah",
			"lastname": "Jameson",
			"email": "jjjameson@gmail.com",
			"password": "spideyisamenace",
			"isAdmin": false
		},
		{
			"firstname": "Otto",
			"lastname": "Octavius",
			"email": "ottoOctopi@gmail.com",
			"password": "docOck15",
			"isAdmin": true
		}
	]
)

//$regex - query operator which allows to find docus that matches the character/pattern of character we are looking for and it looks for docu with partial match and by default is case sensitive

db.users.find({firstname:{$regex: 'O'}})

//$options - added to regex

	// $i option changes criteria to be case insensitive
	db.users.find({firstname:{$regex: 'O', $options: '$i'}})

	// $i option also used to find partial matches
	db.products.find({name:{$regex: 'phone', $options: '$i'}})

	// $i used to find email with the word "web" 
	db.users.find({email:{$regex: 'web', $options: '$i'}})

// mini-activity
db.products.find({name:{$regex: 'razer', $options: '$i'}})
db.products.find({name:{$regex: 'rakk', $options: '$i'}})


// $or $and - logical operator that works almost the same way as they in JS where or is used to find docu that will satisfy at least 1 of the given conditions

db.products.find({$or:[{name:{$regex: 'x',$options: '$i'}},{price:{$lte:10000}}]})


// $and - look or find docu that satisfies both conditions

db.products.find({$and:[{name:{$regex: 'razer',$options: '$i'}},{price:{$gte:3000}}]})
db.products.find({$and:[{name:{$regex: 'x',$options: '$i'}},{price:{$gte:30000}}]})
db.users.find({$and:[{lastname:{$regex: 'w',$options: '$i'}},{isAdmin:false}]})
db.users.find({$and:[{firstname:{$regex: 'a',$options: '$i'}},{isAdmin:true}]})

// projection
/*
	- allows to show/hide certain properties/fields
	- db.collection.find({query},{projection})
	- 0 means hide, 1 means show
	- _id field must be explicitly hidden if you want to hide it
	- we can also just pick which fields to show
*/

db.users.find({},{_id:0,password:0})

db.users.find({isAdmin:true},{_id:0,email:1})

db.users.find({isAdmin:false},{firstname:1,lastname:1})

db.products.find({price:{$gte:10000}},{_id:0,name:1,price:1})