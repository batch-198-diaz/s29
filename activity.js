activity.js

// CREATE COURSE COLLECTION WITH 5 DOCUMENTS
db.courses.insertMany(
	[
		{
			"name": "HTML Basics",
			"price": 20000,
			"isActive": true,
			"instructor": "Sir Alvin"
		},
		{
			"name": "CSS 101 + Flexbox",
			"price": 21000,
			"isActive": true,
			"instructor": "Sir Alvin"
		},
		{
			"name": "Javascript 101",
			"price": 32000,
			"isActive": true,
			"instructor": "Ma'am Tine"
		},
		{
			"name": "Git 101, IDE and CLI",
			"price": 19000,
			"isActive": false,
			"instructor": "Ma'am Tine"
		},
		{
			"name": "React.Js 101",
			"price": 25000,
			"isActive": true,
			"instructor": "Ma'am Miah"
		}
	]
)


// SHOWING NAME AND PRICE (instructor: "Sir Alvin" AND price: gte 20000)
db.courses.find(

		{$and:
			[{instructor:"Sir Alvin"},
			{price:{$gte:20000}}]
		},
		{_id:0,name:1,price:1}
	)

// SHOWING NAME AND PRICE (instructor: "Ma'am Tine" AND isActive: false)
db.courses.find(

		{$and:
			[{instructor:"Ma'am Tine"},
			{isActive:false}]
		},
		{_id:0,name:1,price:1}
	)


// FIND name with 'r' and price: lte 25000
db.courses.find({$and:[{name:{$regex: 'r',$options: '$i'}},{price:{$lte:25000}}]})


// UPDATE price < 21000 to isActive: false
db.courses.updateMany({price:{$lt:21000}},{$set:{"isActive":false}})


// DELETE price >= 25000
db.courses.deleteMany({price:{$gte:25000}})